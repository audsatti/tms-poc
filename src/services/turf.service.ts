import { Injectable } from '@angular/core';

import * as turf from '@turf/turf';

@Injectable({
  providedIn: 'root'
})
export class TurfService {

  constructor() { }

  /** Corta uma geometria com outra geometria
   * @param baseGeometry geometria a ser cortada
   * @param cutterGeometry geometria de corte
   * @param forceReturnGeometry true: Retorna a baseGeometry se não houver cortes | false: retorna null se não houver cortes
   */
  cutGeometry(baseGeometry, cutterGeometry, forceReturnGeometry = false) {
    return turf.difference(baseGeometry, cutterGeometry) || (forceReturnGeometry ? baseGeometry : null);
  }

  /** Itera todos os polígonos da da primeira feature e checa intersecções com a segunda feature
  * @param area Feature ou Geometria com um polígono ou multipoligono
  * @param basePolygon Feature ou Geometria com um polígono sobre o qual será calculado intersecções
  * @returns polígono ou multi-polígono com as intersecções
  */
  private calculateIntersections(area, basePolygon) {
    if (area.type === 'Feature') {
      area = area.geometry;
    }
    if (basePolygon.type === 'Feature') {
      basePolygon = basePolygon.geometry;
    }
    if (basePolygon.type !== 'Polygon') {
      console.error('A área de base (segundo parâmetro) não é um polígono.', [area, basePolygon]);
      return area;
    }
    if (area.type === 'MultiPolygon') {
      const polygonCoordinates = [];
      area.coordinates.forEach(nap => {
        nap = turf.polygon(nap);
        if (turf.intersect(nap, basePolygon)) {
          polygonCoordinates.push(turf.intersect(nap, basePolygon).geometry.coordinates);
        }
      });
      area = turf.multiPolygon(polygonCoordinates);
    } else if (area.type === 'Polygon') {
      area = turf.intersect(area, basePolygon);
    } else {
      console.error('A área informada não é um polígono ou multi-polígono.', [area, basePolygon]);
    }
    return area;
  }

  /** Checa intersecções entre duas geometrias (polígono ou multipolígono)
   * @param geometry1 geometria a ser comparada
   * @param geometry2 geometria a ser comparada
   * @param forceReturnGeometry true: Retorna a geometry1 se não houver intersecçoes | false: retorna null se não houver intersecçoes
   * @returns há interseções ? a área com intersecções : a geometria do parâmetro geometry1
  */
  getIntersections(geometry1, geometry2) {
    /** Geometria final */
    let newGeometry;

    /* Se a geometria da área original for multi-polígono, itera sobre todos os polígonos da área, calcula intersecções com a nova área
        e apara o que sobrar da nova área. Senão apenas calcula a intersecção com a nova área */
    if (geometry2.geometry.type === 'MultiPolygon') {
      const processedGeometry = [];
      geometry2.geometry.coordinates.forEach(oap => {
        oap = turf.polygon(oap);
        /* Se a nova área também for também multi-polígono, itera sobre todos os polígonos da nova área, calcula intersecções sobre
        o polígono da área original e apara do polígono da nova área. Senão apenas calcula a intersecção com o polígono da nova área*/
        processedGeometry.push(this.calculateIntersections(geometry1, oap));
      });
      newGeometry = this.union(processedGeometry.filter(e => e));
    } else if (geometry2.geometry.type === 'Polygon') {
      newGeometry = this.calculateIntersections(geometry1, geometry2);
    }

    return newGeometry;
  }

  /** Mescla um conjunto de uma ou mais geometrias ou features
   * @param geometries array de geometrias ou features
   * @returns uma única feature de polygon ou multi-polygon com as junções
   */
  union(geometries: any[]) {

    if (!geometries.length) { return null; }

    /** Lista de poligonos a serem mesclados */
    const polygonsToMerge = [];

    /*Itera sobre cada geometria informada*/
    geometries.forEach(geometry => {
      /*Verifica se a geometria é na verdade uma feature e extrai a geometria de fato*/
      if (geometry.type === 'Feature') {
        geometry = geometry.geometry;
      }
      if (geometry.type === 'MultiPolygon') { // Se a geometria for multi-poligon, extrai cada polígono e guarda na lista
        geometry.coordinates.forEach(polygon => {
          polygonsToMerge.push(turf.polygon(polygon));
        });
      } else if (geometry.type === 'Polygon') { // Se for um polígono apenas o armazena na lista
        polygonsToMerge.push(turf.polygon(geometry.coordinates));
      }
    });

    return turf.union.apply(null, polygonsToMerge);
  }

  newPolygon(geometry: any) {
    return turf.polygon(geometry);
  }

  bbox(geometry) {
    return turf.bbox(geometry);
  }

}
