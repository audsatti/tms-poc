import { Injectable } from '@angular/core';
import { Collection, Feature, Map, View } from 'ol';
import LayerSwitcher from 'ol-layerswitcher';
import { Control, defaults as defaultControls, OverviewMap } from 'ol/control';
import { Coordinate } from 'ol/coordinate';
import { Extent } from 'ol/extent';
import { FeatureLike } from 'ol/Feature';
import FormatGeoJSON, { GeoJSONFeature, GeoJSONGeometry } from 'ol/format/GeoJSON';
import FormatKML from 'ol/format/KML';
import FormatWKT from 'ol/format/WKT';
import Geometry from 'ol/geom/Geometry';
import GeometryType from 'ol/geom/GeometryType';
import MultiPoint from 'ol/geom/MultiPoint';
import { defaults as defaultInteractions } from 'ol/interaction';
import DrawInteraction from 'ol/interaction/Draw';
import ModifyInteraction from 'ol/interaction/Modify';
import SelectInteraction from 'ol/interaction/Select';
import SnapInteraction from 'ol/interaction/Snap';
import LayerGroup from 'ol/layer/Group';
import ImageLayer from 'ol/layer/Image';
import Layer from 'ol/layer/Layer';
import TileLayer from 'ol/layer/Tile';
import VectorLayer from 'ol/layer/Vector';
import { fromLonLat } from 'ol/proj';
import ImageSource from 'ol/source/Image';
import ImageStatic from 'ol/source/ImageStatic';
import RasterSource from 'ol/source/Raster';
import TileSource from 'ol/source/Tile';
import VectorSource from 'ol/source/Vector';
import SourceXYZ from 'ol/source/XYZ';
import StyleCircle from 'ol/style/Circle';
import StyleFill from 'ol/style/Fill';
import StyleStroke from 'ol/style/Stroke';
import Style, { StyleLike } from 'ol/style/Style';

@Injectable({
  providedIn: 'root'
})
export class OpenlayerService {

  private DEFAULT_PROJECTION = 'EPSG:4326';
  private FORMAT_GEO_JSON = new FormatGeoJSON();
  private FORMAT_WKT = new FormatWKT();
  private FORMAT_KML = new FormatKML();

  /** Cores pré definidas para os vetores no mapa */
  vectorColors = {
    default: '255, 170, 0',
    approved: '50, 235, 20',
    gray: '200, 200, 200',
    red: '255, 48, 48',
    green: '0, 137, 0',
    white: '255, 255, 255'
  };

  constructor() { }

  /** Constrói um componente de mapa (openlayers)
   * @param targetId Id da tag que será o mapa
   * @returns Um mapa do open layers
  */
  constructMap(targetId: string): Map {

    /** Origem do mapa vetorial */
    const mapBoxSource = new SourceXYZ({
      crossOrigin: 'anonymous',
      url: 'https://{a-c}.tile.openstreetmap.org/{z}/{x}/{y}.png',
      attributions: '© <a href="https://www.openstreetmap.org/copyright">' +
        'OpenStreetMap contributors</a>'
    });

    /** Camada do mapa vetorial */
    const mapBoxLayer = new TileLayer({
      // projection: 'EPSG:4326',
      source: mapBoxSource
    });

    mapBoxLayer.set('title', 'Open Street Maps');

    /** Sub camadas do mapa vetorial */
    const layers = [
      new LayerGroup({
        layers: [mapBoxLayer]
      })
    ];

    layers[0].set('title', 'Base Maps');

    const view = new View({
      projection: 'EPSG:4326',
      center: fromLonLat([-50, -15], 'EPSG:4326'),
      zoom: 4.2,
      minZoom: 4.2,
      maxZoom: 20
    });

    const map = new Map({
      controls: defaultControls().extend([
        new OverviewMap()
      ]),
      target: targetId,
      layers: layers,
      interactions: defaultInteractions({
        doubleClickZoom: false
      }),
      view: view
    });

    /** Botão com a lista para seleçã de camadas */
    const layerSwitcher = new LayerSwitcher({
      tipLabel: 'Controle de Camadas' // Optional label for button
    });

    map.addControl(layerSwitcher);

    return map;
  }

  /** Retorna um objeto XYZ */
  newXYZ(url: string) {
    return new SourceXYZ({ 
      url,
      crossOrigin: 'anonymous'
    });
  }

  /** Retorna os nós de uma geometria */
  getGeometryNodes(feature: Feature): Coordinate[] {

    const allCoordinates = feature.getGeometry()['getCoordinates']();
    const pointsCollection = [];

    for (let c = 0; c < allCoordinates.length; c++) {
      const coordinatesCollection = feature.getGeometry()['getCoordinates']()[c];
      for (let co = 0; co < coordinatesCollection.length; co++) {
        if (coordinatesCollection[co].length === 2) {
          pointsCollection.push(coordinatesCollection[co]);
        } else {
          for (let intern = 0; intern < coordinatesCollection[co].length; intern++) {
            pointsCollection.push(coordinatesCollection[co][intern]);
          }
        }
      }
    }

    return pointsCollection;
  }

  /** Aplica estilo a uma feature
   * @param feature Feature a ser processada com o estilo
   * @param color A cor RGB do vetor: '55, 55, 55'
   * @param role O tipo do estilo de acordo com a função/comportamento
   * @returns um ol.style.Style de acordo com os parâmetros da feature
  */
  getStyle(feature: FeatureLike, color: string, role: 'static' | 'editable' | 'drawing' | 'selected'): any {

    const nodes = this.getGeometryNodes(feature as Feature);

    let radius, lineDash, opacity;

    if (role === 'static') {
      radius = null;
      lineDash = [0, 0];
      opacity = 0.2;
    } else if (role === 'editable') {
      radius = 2;
      lineDash = [0, 0];
      opacity = 0.3;
    } else if (role === 'drawing') {
      radius = null;
      lineDash = [5, 3];
      opacity = 0.4;
    } else if (role === 'selected') {
      radius = 4;
      lineDash = [5, 3];
      opacity = 0.7;
    }

    const styles = {};

    const image = new StyleCircle({
      radius: 5,
      fill: null,
      stroke: new StyleStroke({ color: 'rgba(' + color + ', .3)', width: 5 })
    });

    const glebaStyles = [
      new Style({
        stroke: new StyleStroke({
          color: 'rgba(' + color + ', .7)',
          width: 2,
          lineDash: lineDash,
        }),
        fill: new StyleFill({
          color: 'rgba(' + color + ', ' + opacity + ')'
        }),

      }),
      new Style({
        image: new StyleCircle({
          stroke: new StyleStroke({
            color: 'rgba(' + color + ', .4)',
            width: 7
          }),
          radius: radius,
          fill: new StyleFill({
            color: 'rgba(' + color + ', ' + opacity + ')'
          })
        }),
        geometry: new MultiPoint(nodes)
      }),
    ];

    styles['Point'] = new Style({ image: image });

    styles['Polygon'] = glebaStyles;

    styles['MultiLineString'] = new Style({
      stroke: new StyleStroke({
        color: 'blue',
        width: 3
      })
    });

    styles['MultiPolygon'] = glebaStyles;

    styles['default'] = new Style({
      stroke: new StyleStroke({
        color: 'rgba(' + color + ', 1.0)',
        width: 2,
        lineDash: [2, 3]
      }),
      fill: new StyleFill({
        color: 'rgba(' + color + ', .5)'
      }),
      image: image
    });

    return styles[feature.getGeometry().getType()] || styles['default'];
  }

  /**
   * Converte um objeto openlayers em um GeoJSON
   * @returns formatador Geo JSON estático do open layers
   */
  formatGeoJSON(): FormatGeoJSON {
    return this.FORMAT_GEO_JSON;
  }

  /**
   * Converte um objeto openlayers em um WKT
   * @returns formatador WKT estático do open layers
   */
  formatWKT(): FormatWKT {
    return this.FORMAT_WKT;
  }

  /**
   * Converte um objeto openlayers em um KML
   * @returns formatador KML estático do open layers
   */
  formatKML(): FormatKML {
    return this.FORMAT_KML;
  }

  /** Converte um objeto geometria em um objeto feature
   * @param geometry objeto geometria
   * @param name propriedade name da feature
   * @returns objeto feature
   */
  geometryToFeature(geometry: GeoJSONGeometry, name = ''): GeoJSONFeature {
    return {
      'type': 'Feature',
      'properties': {
        'name': name,
      },
      'geometry': geometry
    };
  }

  /**
   * Cria um botão/controle para o mapa
   * @param name Um nome para o botão, um id
   * @param opt_options Opções do olControl: {title, classRef, keyCode}
   * @param funcRef Referência à função adicionada no controle: this
   * @param color A cor RGB do botão
   * @param func Função a ser executada pelo botão
   */
  createControlButton(name: string, opt_options, funcRef: Function, color: string = '0,60,136', func: () => any): void {

    /** Elemento html do botão do controle */
    const button = document.createElement('button');
    /** Elemento html da div que engloba o botão do controle */
    const element = document.createElement('div');

    button.id = 'ctrl' + name;
    button.addEventListener('click', func, false);
    button.addEventListener('touchstart', func, false);
    button.style.backgroundColor = `rgba(${color},.5)`;

    element.className = 'edition-geometry ctrl' + name + ' ol-unselectable ol-control';
    element.title = opt_options.title || null;
    element.appendChild(button);

    if (opt_options.keyCode) {
      const keyDownFunc = e => (e.keyCode !== opt_options.keyCode || func());
      document.addEventListener('keydown', keyDownFunc, false);
    }

    Control.call(funcRef, {
      element: element,
      target: opt_options.target
    });
  }

  /** Aplica um controle ao mapa
   * @param map Referência ao objeto do mapa ol.Map
   * @param olControl Referência à função do controle
   * @param classRef Referência à classe do componente
   * @param title Título do botão
   * @param keyCode Código da tecla atalho
   * @returns Um controle open layers
  */
  /*
   setMapControl(map: Map, olControl, classRef, title: string, keyCode: number = null) {
     ol.inherits(olControl, Control);
     const control = new olControl({
       classRef: classRef,
       title: title,
       keyCode: keyCode
     });
     map.addControl(control);
     return control;
   }
   */

  /** @returns Um objeto source/Vector do openlayers (origem) */
  newSourceVector(): VectorSource {
    return new VectorSource();
  }

  /** Gera um objeto layer/Vector do openlayers (camada)
   * @param title o título da camada
   * @param source o objeto origem da camada
   * @param style um objeto style do openlayers
   * @returns Um objeto layer/Vector do openlayers (camada)
  */
  newVectorLayer(title?: string, source?: VectorSource, style?: StyleLike): VectorLayer {
    const vectorLayer = new VectorLayer({
      // projection: this.DEFAULT_PROJECTION,
      source,
      style
    });

    vectorLayer.set('title', title);

    return vectorLayer;
  }

  /** Gera um objeto layer/Vector do openlayers (camada)
   * @param title o título da camada
   * @param source o objeto origem da camada
   * @param style um objeto style do openlayers
   * @returns Um objeto layer/Vector do openlayers (camada)
  */
  newLayerTile(title?: string, source?: TileSource): TileLayer {
    const tileLayer = new TileLayer({
      // projection: this.DEFAULT_PROJECTION,
      source
    });

    tileLayer.set('title', title);

    return tileLayer;
  }

  /** Gera um objeto layer/Group do openlayers (grupo de camadas)
   * @param title o título do grupo
   * @param layers uma lista de Layers para inserir no grupo (lista de camadas)
   * @returns um objeto layer/Group do openlayers (grupo de camadas)
  */
  newLayerGroup(title: string, layers: Layer[]): LayerGroup {
    const layerGroup = new LayerGroup({
      layers: layers
    });

    layerGroup.set('title', title);

    return layerGroup;
  }

  /** Gera um objeto de layer/Image do openlayers (camada de imagem)
   * @param title o título da camada
   * @param url url da imagem
   * @param imageExtent um objeto com as coordenadas [minX, minY, maxX, maxY]
   * @returns um objeto de layer/Image do openlayers (camada de imagem)
  */
  newLayerImage(title: string, url: string, imageExtent: Extent): ImageLayer {
    const imageLayer = new ImageLayer({
      source: new ImageStatic({
        attributions: '© <a href="http://xkcd.com/license.html">xkcd</a>',
        url,
        crossOrigin: 'anonymous',
        projection: this.DEFAULT_PROJECTION,
        imageExtent
      })
    });

    imageLayer.set('title', title);

    return imageLayer;
  }

  /** Gera um objeto de layer/Raster do openlayers para filtro VGI
   * @param imageSource um objeto source/Image ou source/XYZ do openlayers
   * @param title título da camada
   * @returns um objeto layer/Image do openlayers (camada de imagem)
  */
  newVGIRasterLayer(imageSource: ImageSource | SourceXYZ, title: string): ImageLayer {

    const colorRampThesholdsBase = () => [-0.1, -0.08, -0.05, 0, 100];
    const colorRampBase = () => [
      [215, 25, 28],
      [253, 174, 97],
      [255, 255, 192],
      [166, 217, 106],
      [26, 150, 65]
    ];

    const vgi = (pixel: number[] | ImageData) => {
      const r = pixel[0] / 255;
      const g = pixel[1] / 255;
      const b = pixel[2] / 255;
      return (g - r) / (g + r);
      return (2 * g - r - b) / (2 * g + r + b);
    }

    const raster = new RasterSource({
      sources: [imageSource],
      operation: (pixels, data: any) => {
        var pixel = pixels[0];
        var value = vgi(pixel);

        const colorRampThesholds = colorRampThesholdsBase();
        const colorRamp = colorRampBase();
        
        if (!isNaN(value)) {
          for (let x = 0; x <= colorRampThesholds.length - 1; x++) {
            if (value <= colorRampThesholds[x]) {
              pixel[0] = colorRamp[x][0];
              pixel[1] = colorRamp[x][1];
              pixel[2] = colorRamp[x][2];
              break
            }
          }
          pixel[3] = 255;
        }
        
        return pixel;
      },
      lib: {
        vgi,
        colorRampThesholdsBase,
        colorRampBase
      },
    });

    const rasterImageLayer = new ImageLayer({ source: raster });

    rasterImageLayer.set('title', title);

    return rasterImageLayer;

  }

  /* ------------- INTERAÇÕES ------------- */

  /** Gera uma função de desenho de geometrias para um mapa openlayers
   * @param type o tipo da geometria a ser desenhada
   * @param source um objeto source/Vector do openlayers onde a interação atuará: source/Vector
   * @param style o estilo do vetor: StyleLike
   * @returns uma função de desenho de geometrias DrawInteraction
  */
  newInteractionDraw(type: GeometryType, source: VectorSource, style?: StyleLike): DrawInteraction {
    return new DrawInteraction({
      source: source,
      type: type,
      style: style
    });
  }

  /** Gera uma função de snap para um mapa openlayers
   * @param source um objeto layer/Vector do openlayers onde a interação atuará: source/Vector
   * @param features Feature ou FeatureColection onde onde a interação atuará
   * @returns uma função de snap
  */
  newInteractionSnap(source?: VectorSource<Geometry>, features?: Collection<Feature<Geometry>>): SnapInteraction {
    return new SnapInteraction({
      source: source,
      features: features
    });
  }

  /** Gera uma função de edição de geometria para um mapa openlayers
   * @param source um objeto layer/Vector do openlayers onde a interação atuará: source/Vector
   * @param features Feature ou FeatureColection onde onde a interação atuará
   * @returns uma função de edição de geometria
  */
  newInteractionModify(source?: VectorSource<Geometry>, features?: Collection<Feature<Geometry>>): ModifyInteraction {
    return new ModifyInteraction({
      features: features,
      source: source
    });
  }

  newInteractionSelect(style?: StyleLike): SelectInteraction {
    return new SelectInteraction({
      style: style
    });
  }

}
