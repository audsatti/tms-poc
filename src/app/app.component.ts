import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Map } from 'ol';
import { eosLandsat8Bands, eosLandsat8Images } from 'src/data/eos-landsat8';
import { eosSentinel2Bands, eosSentinel2Images } from 'src/data/eos-sentinel2';
import { field } from 'src/data/field';
import { planetImages } from 'src/data/planet-images';
import { OpenlayerService } from 'src/services/openlayer.service';
import { TurfService } from 'src/services/turf.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [
    DatePipe
  ]
})
export class AppComponent implements OnInit {

  map: Map;

  originalSource;
  originalLayer;
  fieldLayers;
  imageLayer;

  planetImages = planetImages;
  eosLandsat8Images = eosLandsat8Images;
  eosLandsat8Bands = eosLandsat8Bands;
  eosSentinel2Images = eosSentinel2Images;
  eosSentinel2Bands = eosSentinel2Bands;

  eosToken = 'apk.46945e8aa0ddbe4baaabf22f3475acbe935d5cc5408cd8ff382c069f18a00941';

  compositions = [
    {
      planet: 'B3,B2,B1',
      eosSentinel2: 'B4,B3,B2',
      eosLandsat8: 'B4,B3,B2'
    },
    {
      planet: 'B3,B2,B1',
      eosSentinel2: 'B5,B6,B4',
      eosLandsat8: 'B8A,B11,B4'
    },
    {
      planet: 'B4,B3,B2',
      eosSentinel2: 'B5,B4,B3',
      eosLandsat8: 'B8,B4,B3'
    }
  ]

  currentComposition:
  {
    planet: string,
    eosSentinel2: string,
    eosLandsat8: string
  } = {} as any;

  imageSource: 'planet' | 'eosLandsat8' | 'eosSentinel2' = 'planet';

  constructor(
    private olService: OpenlayerService,
    private datePipe: DatePipe,
    private turfService: TurfService
  ) { }

  ngOnInit() {
    this.map = this.olService.constructMap('map');
    this.currentComposition = this.compositions[0];
    this.initMap();
  }

  initMap() {

    this.originalSource = this.olService.newSourceVector();

    this.originalLayer = this.olService.newVectorLayer(
      'Gleba Original',
      this.originalSource,
      feature => this.olService.getStyle(feature, this.olService.vectorColors.default, 'editable')
    );

    this.fieldLayers = this.olService.newLayerGroup('Glebas', [this.originalLayer]);

    this.map.addLayer(this.fieldLayers);

    const formattedFeature = this.olService.formatGeoJSON().readFeature(field.geometry);

    this.originalSource.addFeature(formattedFeature.clone());
    this.originalLayer.setZIndex(3);

    this.setMapImage(planetImages[0])

    this.fitMap();
  }

  setMapImage(image: { id: string, date: string }) {
    if(this.imageLayer) {
      this.map.removeLayer(this.imageLayer);
    }

    const mosaicTitle = `PLANET - ${this.datePipe.transform(image.date, 'dd/MM/yyyy')}`;

    const extent = this.turfService.bbox(field.boundingBox);

    const aerial = this.olService.newXYZ(this.getImageURLForMap(image));

    const LayerMosaic = this.olService.newLayerTile(mosaicTitle, aerial);

    const rasterLayer = this.olService.newVGIRasterLayer(aerial, `VGI: ${mosaicTitle}`);

    this.imageLayer = this.olService.newLayerGroup('Mosaico', [LayerMosaic, rasterLayer]);

    this.imageLayer.setZIndex(2);

    this.map.addLayer(this.imageLayer);
  }

  fitMap() {
    this.map.updateSize();
    this.map.getView().fit(
      this.originalSource.getFeatures()[0].getGeometry(),
      { size: this.map.getSize(), duration: 0, padding: [50,50,50,50] }
    );
  }

  getImageURLForMap(image): string {
    let link;
    switch (this.imageSource) {
      case 'planet':
        link = `https://tiles{0-3}.planet.com/data/v1/PSScene4Band/${image.id}/{z}/{x}/{y}.png?api_key=9ccd653f207c45ee9f6c9e893a47c11e`;
        break;
      case 'eosLandsat8':
        link = `https://{a-d}-gate.eos.com/api/render/${image.id}/${this.currentComposition.eosLandsat8}/{z}/{x}/{y}?api_key=${this.eosToken}`;
        break;
      case 'eosSentinel2':
        link = `${image.tms}?api_key=${this.eosToken}`.replace('{band}', `${this.currentComposition.eosSentinel2}`);
        break;
    }

    return link;
  }

  changeComposition(event) {
    this.currentComposition = this.compositions[event.target.value];
  }

  getselectedImagesSet() {
    switch (this.imageSource) {
      case 'planet': return planetImages;
      case 'eosLandsat8': return eosLandsat8Images;
      case 'eosSentinel2': return eosSentinel2Images;
    }
  }
}
