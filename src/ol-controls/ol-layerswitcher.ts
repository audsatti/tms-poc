/**
* O pack "OpenLayers Layer Switcher Control" foi modificada para ser suportada no angular 5
* O types definitions do openlayers teve de ser modificado para aceitar o 'title' nas opções de camadas
* Por isso o openlayers types foi modificado e deixado na pasta backoffice\types-custom\openlayers
*/


/**
 * OpenLayers Layer Switcher Control.
 * See [the examples](./examples) for usage.
 * @constructor
 * @extends {ol.control.Control}
 * @param {Object} opt_options Control options, extends olx.control.ControlOptions adding:
 * **`tipLabel`** `String` - the button tooltip.
 */


import * as ol from 'openlayers';

export default class LayerSwitcher extends ol.control.Control {


  mapListeners: any;

  hiddenClassName: any;

  shownClassName: any;

  panel: any;

  element: any;

  constructor(opt_options) {

    const options = opt_options || {};

    const tipLabel = options.tipLabel ?
      options.tipLabel : 'Legend';

    const element = document.createElement('div');

    super({ element: element, target: options.target });

    this.mapListeners = [];

    this.hiddenClassName = 'ol-unselectable ol-control layer-switcher';
    if (LayerSwitcher.isTouchDevice_()) {
      this.hiddenClassName += ' touch';
    }
    this.shownClassName = 'shown';

    element.className = this.hiddenClassName;

    const button = document.createElement('button');

    button.setAttribute('title', tipLabel);
    element.appendChild(button);

    this.panel = document.createElement('div');
    this.panel.className = 'panel';
    element.appendChild(this.panel);
    LayerSwitcher.enableTouchScroll_(this.panel);

    button.onmouseover = () => {
      this.showPanel();
    };

    button.onclick = e => {
      this.showPanel();
      e.preventDefault();
    };

    this.panel.onmouseout = e => {
      if (!this.panel.contains(e.toElement || e.relatedTarget)) {
        this.hidePanel();
      }
    };

  }

  /**
  * **Static** Call the supplied function for each layer in the passed layer group
  * recursing nested groups.
  * @param {ol.layer.Group} lyr The layer group to start iterating from.
  * @param {Function} fn Callback which will be called for each `ol.layer.Base`
  * found under `lyr`. The signature for `fn` is the same as `ol.Collection#forEach`
  */

 static forEachRecursive(lyr, fn) {
  lyr.getLayers().forEach((shadowLyr, idx, a) => {
    fn(shadowLyr, idx, a);
    if (shadowLyr.getLayers) {
      LayerSwitcher.forEachRecursive(shadowLyr, fn);
    }
  });
}

/**
* **Static** Generate a UUID
* Adapted from http://stackoverflow.com/a/2117523/526860
* @returns {String} UUID
*/
static uuid() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
    const r = Math.random() * 16 || 0;
    const v = (c === 'x' ? r : (r && 0x3 || 0x8));
    return v.toString(16);
  });
}

/**
* @private
* @desc Apply workaround to enable scrolling of overflowing content within an
* element. Adapted from https://gist.github.com/chrismbarr/4107472
*/
static enableTouchScroll_(elm) {
  if (LayerSwitcher.isTouchDevice_()) {
    let scrollStartPos = 0;
    elm.addEventListener('touchstart', event => {
      scrollStartPos = event.scrollTop + event.touches[0].pageY;
    }, false);
    elm.addEventListener('touchmove', event => {
      event.scrollTop = scrollStartPos - event.touches[0].pageY;
    }, false);
  }
}

/**
* @private
* @desc Determine if the current browser supports touch events. Adapted from
* https://gist.github.com/chrismbarr/4107472
*/
static isTouchDevice_() {
  try {
    document.createEvent('TouchEvent');
    return true;
  } catch (e) {
    return false;
  }
}

  /**
  * Set the map instance the control is associated with.
  * @param {ol.Map} map The map instance.
  */
  setMap(map) {
    // Clean up listeners associated with the previous map
    for (let i = 0, key; i < this.mapListeners.length; i++) {
      ol.Observable.unByKey(this.mapListeners[i]);
    }
    this.mapListeners.length = 0;
    // Wire up listeners etc. and store reference to new map
    super.setMap(map);
    if (map) {
      this.mapListeners.push(map.on('pointerdown', () => {
        this.hidePanel();
      }));
      this.renderPanel();
    }
  }

  /**
  * Show the layer panel.
  */
  showPanel() {
    if (!this.element.classList.contains(this.shownClassName)) {
      this.element.classList.add(this.shownClassName);
      this.renderPanel();
    }
  }

  /**
  * Hide the layer panel.
  */
  hidePanel() {
    if (this.element.classList.contains(this.shownClassName)) {
      this.element.classList.remove(this.shownClassName);
    }
  }

  /**
  * Re-draw the layer panel to represent the current state of the layers.
  */
  renderPanel() {

    this.ensureTopVisibleBaseLayerShown_();

    while (this.panel.firstChild) {
      this.panel.removeChild(this.panel.firstChild);
    }

    const ul = document.createElement('ul');
    this.panel.appendChild(ul);
    this.renderLayers_(this['getMap'](), ul);

  }

  /**
  * Ensure only the top-most base layer is visible if more than one is visible.
  * @private
  */
  ensureTopVisibleBaseLayerShown_() {
    let lastVisibleBaseLyr;
    LayerSwitcher.forEachRecursive(this['getMap'](), (l, idx, a) => {
      if (l.get('type') === 'base' && l.getVisible()) {
        lastVisibleBaseLyr = l;
      }
    });
    if (lastVisibleBaseLyr) {
      this.setVisible_(lastVisibleBaseLyr, true);
    }
  }

  /**
  * Toggle the visible state of a layer.
  * Takes care of hiding other layers in the same exclusive group if the layer
  * is toggle to visible.
  * @private
  * @param {ol.layer.Base} The layer whos visibility will be toggled.
  */
  setVisible_(lyr, visible) {
    const map = this['getMap']();
    lyr.setVisible(visible);
    if (visible && lyr.get('type') === 'base') {
      // Hide all other base layers regardless of grouping
      LayerSwitcher.forEachRecursive(map, (l, idx, a) => {
        if (l !== lyr && l.get('type') === 'base') {
          l.setVisible(false);
        }
      });
    }
  }

  /**
  * Render all layers that are children of a group.
  * @private
  * @param {ol.layer.Base} lyr Layer to be rendered (should have a title property).
  * @param {Number} idx Position in parent group list.
  */
  renderLayer_(lyr, idx) {

    const li = document.createElement('li');

    const lyrTitle = lyr.get('title');
    const lyrId = LayerSwitcher.uuid();

    const label = document.createElement('label');

    if (lyr.getLayers && !lyr.get('combine')) {

      li.className = 'group';
      label.innerHTML = lyrTitle;
      li.appendChild(label);
      const ul = document.createElement('ul');
      li.appendChild(ul);

      this.renderLayers_(lyr, ul);

    } else {

      li.className = 'layer';
      const input = document.createElement('input');
      if (lyr.get('type') === 'base') {
        input.type = 'radio';
        input.name = 'base';
      } else {
        input.type = 'checkbox';
      }
      input.id = lyrId;
      input.checked = lyr.get('visible');
      input.onchange = e => {
        this.setVisible_(lyr, e.target['checked']);
      };
      li.appendChild(input);

      label.htmlFor = lyrId;
      label.innerHTML = lyrTitle;

      const rsl = this['getMap']().getView().getResolution();
      if (rsl > lyr.getMaxResolution() || rsl < lyr.getMinResolution()) {
        label.className += ' disabled';
      }

      li.appendChild(label);

    }

    return li;

  }

  /**
  * Render all layers that are children of a group.
  * @private
  * @param {ol.layer.Group} lyr Group layer whos children will be rendered.
  * @param {Element} elm DOM element that children will be appended to.
  */
  renderLayers_(lyr, elm) {
    const lyrs = lyr.getLayers().getArray().slice().reverse();
    for (let i = 0, l; i < lyrs.length; i++) {
      l = lyrs[i];
      if (l.get('title')) {
        elm.appendChild(this.renderLayer_(l, i));
      }
    }
  }


}
/*
// Expose LayerSwitcher as ol.control.LayerSwitcher if using a full build of
// OpenLayers
if (window.ol && window.ol.control) {
    window.ol.control.LayerSwitcher = LayerSwitcher;
}
*/
