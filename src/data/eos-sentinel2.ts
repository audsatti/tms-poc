export const eosSentinel2Bands = [
    {
        flag: false,
        name: 'B01'
    },
    {
        flag: false,
        name: 'B02'
    },
    {
        flag: false,
        name: 'B03'
    },
    {
        flag: true,
        name: 'B04'
    },
    {
        flag: true,
        name: 'B05'
    },
    {
        flag: true,
        name: 'B06'
    },
    {
        flag: false,
        name: 'B07'
    },
    {
        flag: false,
        name: 'B08'
    },
    {
        flag: false,
        name: 'B09'
    },
    {
        flag: false,
        name: 'B10'
    },
    {
        flag: false,
        name: 'B11'
    },
    {
        flag: false,
        name: 'B12'
    },
    {
        flag: false,
        name: 'B8A'
    },
]

export const eosSentinel2Images = [
	{
		"view_id": "S2/22/K/FF/2019/12/9/0",
		"sceneID": "S2B_tile_20191209_22KFF_0",
		"tms": "https://gate.eos.com/api/render/S2/22/K/FF/2019/12/9/0/{band}/{z}/{x}/{y}",
		"cloudCoverage": 98.76,
		"date": "2019-12-09"
	},
	{
		"view_id": "S2/22/K/FF/2019/12/14/0",
		"sceneID": "S2A_tile_20191214_22KFF_0",
		"tms": "https://gate.eos.com/api/render/S2/22/K/FF/2019/12/14/0/{band}/{z}/{x}/{y}",
		"cloudCoverage": 100,
		"date": "2019-12-14"
	},
	{
		"view_id": "S2/22/K/FF/2019/12/19/0",
		"sceneID": "S2B_tile_20191219_22KFF_0",
		"tms": "https://gate.eos.com/api/render/S2/22/K/FF/2019/12/19/0/{band}/{z}/{x}/{y}",
		"cloudCoverage": 98.77,
		"date": "2019-12-19"
	},
	{
		"view_id": "S2/22/K/FF/2019/12/24/0",
		"sceneID": "S2A_tile_20191224_22KFF_0",
		"tms": "https://gate.eos.com/api/render/S2/22/K/FF/2019/12/24/0/{band}/{z}/{x}/{y}",
		"cloudCoverage": 3.85,
		"date": "2019-12-24"
	},
	{
		"view_id": "S2/22/K/FF/2019/12/29/0",
		"sceneID": "S2B_tile_20191229_22KFF_0",
		"tms": "https://gate.eos.com/api/render/S2/22/K/FF/2019/12/29/0/{band}/{z}/{x}/{y}",
		"cloudCoverage": 40.34,
		"date": "2019-12-29"
	}
];
