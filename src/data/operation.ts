export const operation = {
    "id": 803,
    "uuid": "37caf7d2-d2d8-4c0e-9197-0c769ee30369",
    "externalId": "8ed92b8f-3085-47b9-8104-ff654e0b920f",
    "client": {
        "name": null,
        "document": "95363157187"
    },
    "fields": [
        {
            "id": 3804,
            "status": "PENDING_TECHNICAL_ANALYSIS",
            "hasConfirmedArea": false,
            "hasConfirmedTemporalAnalysis": false
        }
    ],
    "createdAt": "2019-12-04",
    "status": "PENDING_TECHNICAL_ANALYSIS_TO_CONFIRMATION_AREA"
}
