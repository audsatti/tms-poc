export const field = {
    "id": 3804,
    "culture": {
        "name": "Soja",
        "code": 2
    },
    "geometry": {
        "type": "Polygon",
        "coordinates": [
            [
                [
                    -49.298007,
                    -17.964313
                ],
                [
                    -49.285607,
                    -17.94925
                ],
                [
                    -49.292348,
                    -17.949885
                ],
                [
                    -49.293439,
                    -17.953895
                ],
                [
                    -49.296163,
                    -17.954605
                ],
                [
                    -49.296026,
                    -17.953759
                ],
                [
                    -49.299787,
                    -17.952689
                ],
                [
                    -49.299749,
                    -17.952925
                ],
                [
                    -49.299671,
                    -17.953437
                ],
                [
                    -49.299707,
                    -17.953966
                ],
                [
                    -49.299784,
                    -17.954474
                ],
                [
                    -49.299886,
                    -17.954795
                ],
                [
                    -49.296234,
                    -17.95675
                ],
                [
                    -49.300489,
                    -17.962001
                ],
                [
                    -49.298007,
                    -17.964313
                ]
            ]
        ]
    },
    "boundingBox": {
        "type": "MultiPolygon",
        "coordinates": [
            [
                [
                    [
                        -49.3019772,
                        -17.9658193
                    ],
                    [
                        -49.2841188,
                        -17.9658193
                    ],
                    [
                        -49.2841188,
                        -17.9477437
                    ],
                    [
                        -49.3019772,
                        -17.9477437
                    ],
                    [
                        -49.3019772,
                        -17.9658193
                    ]
                ]
            ]
        ]
    },
    "area": {
        "effectiveAreaInHectares": 88.03398027812243,
        "areaInHectares": 88.03398027812243
    },
    "createdAt": "2019-12-04",
    "mosaics": [
        {
            "id": 102806,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-01/1044-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-01/1044base-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-01/1044-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-01/1044ndvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 0.0,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-09-01",
            "suddenDrop": false,
            "hasCloud": false
        },
        {
            "id": 102800,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-02/101b-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-02/101bbase-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-02/101b-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-02/101bndvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 0.0,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-09-02",
            "suddenDrop": false,
            "hasCloud": false
        },
        {
            "id": 102794,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-03/1032-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-03/1032base-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-03/1032-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-03/1032ndvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 0.0,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-09-03",
            "suddenDrop": false,
            "hasCloud": false
        },
        {
            "id": 102784,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-04/1025-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-04/1025base-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-04/1025-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-04/1025ndvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 0.0,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-09-04",
            "suddenDrop": false,
            "hasCloud": false
        },
        {
            "id": 102783,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-06/0f28-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-06/0f28base-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-06/0f28-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-06/0f28ndvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 0.0,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-09-06",
            "suddenDrop": false,
            "hasCloud": false
        },
        {
            "id": 102790,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-09/1048-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-09/1048base-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-09/1048-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-09/1048ndvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 0.0,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-09-09",
            "suddenDrop": false,
            "hasCloud": false
        },
        {
            "id": 102804,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-10/0f28-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-10/0f28base-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-10/0f28-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-10/0f28ndvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 0.0,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-09-10",
            "suddenDrop": false,
            "hasCloud": false
        },
        {
            "id": 102807,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-13/1027-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-13/1027base-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-13/1027-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-13/1027ndvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 0.0,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-09-13",
            "suddenDrop": false,
            "hasCloud": false
        },
        {
            "id": 102789,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-14/0f28-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-14/0f28base-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-14/0f28-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-14/0f28ndvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 0.0,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-09-14",
            "suddenDrop": false,
            "hasCloud": false
        },
        {
            "id": 102802,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-16/1001-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-16/1001base-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-16/1001-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-16/1001ndvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 0.0,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-09-16",
            "suddenDrop": false,
            "hasCloud": false
        },
        {
            "id": 102776,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-17/1027-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-17/1027base-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-17/1027-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-17/1027ndvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 0.0,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-09-17",
            "suddenDrop": false,
            "hasCloud": false
        },
        {
            "id": 102778,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-20/1003-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-20/1003base-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-20/1003-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-20/1003ndvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 0.6486809571489874,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-09-20",
            "suddenDrop": false,
            "hasCloud": true
        },
        {
            "id": 102785,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-21/1011-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-21/1011base-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-21/1011-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-21/1011ndvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 0.9939820585650939,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-09-21",
            "suddenDrop": false,
            "hasCloud": true
        },
        {
            "id": 102792,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-23/0f49-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-23/0f49base-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-23/0f49-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-23/0f49ndvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 0.0,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-09-23",
            "suddenDrop": false,
            "hasCloud": false
        },
        {
            "id": 102799,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-24/103e-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-24/103ebase-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-24/103e-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-24/103endvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 0.0,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-09-24",
            "suddenDrop": false,
            "hasCloud": false
        },
        {
            "id": 102777,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-27/1035-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-27/1035base-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-27/1035-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-27/1035ndvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 0.41397102396959357,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-09-27",
            "suddenDrop": false,
            "hasCloud": true
        },
        {
            "id": 102787,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-28/1003-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-28/1003base-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-28/1003-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-28/1003ndvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 0.07899953000796943,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-09-28",
            "suddenDrop": false,
            "hasCloud": false
        },
        {
            "id": 102796,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-29/0f44-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-29/0f44base-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-29/0f44-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-09-29/0f44ndvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 0.0,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-09-29",
            "suddenDrop": false,
            "hasCloud": false
        },
        {
            "id": 102775,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-02/0f35-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-02/0f35base-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-02/0f35-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-02/0f35ndvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 0.0,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-10-02",
            "suddenDrop": false,
            "hasCloud": false
        },
        {
            "id": 102795,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-05/1049-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-05/1049base-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-05/1049-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-05/1049ndvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 0.0,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-10-05",
            "suddenDrop": false,
            "hasCloud": false
        },
        {
            "id": 102774,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-07/0f33-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-07/0f33base-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-07/0f33-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-07/0f33ndvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 0.0,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-10-07",
            "suddenDrop": false,
            "hasCloud": false
        },
        {
            "id": 102809,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-15/0f4e-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-15/0f4ebase-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-15/0f4e-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-15/0f4endvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 0.0,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-10-15",
            "suddenDrop": false,
            "hasCloud": false
        },
        {
            "id": 102786,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-16/1052-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-16/1052base-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-16/1052-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-16/1052ndvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 0.0,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-10-16",
            "suddenDrop": false,
            "hasCloud": false
        },
        {
            "id": 102805,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-17/0f15-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-17/0f15base-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-17/0f15-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-17/0f15ndvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 0.0,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-10-17",
            "suddenDrop": false,
            "hasCloud": false
        },
        {
            "id": 102798,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-18/1009-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-18/1009base-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-18/1009-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-18/1009ndvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 0.0,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-10-18",
            "suddenDrop": false,
            "hasCloud": false
        },
        {
            "id": 102797,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-22/1009-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-22/1009base-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-22/1009-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-22/1009ndvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 0.6380652675889409,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-10-22",
            "suddenDrop": false,
            "hasCloud": true
        },
        {
            "id": 102801,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-23/1039-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-23/1039base-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-23/1039-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-23/1039ndvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 0.0,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-10-23",
            "suddenDrop": false,
            "hasCloud": false
        },
        {
            "id": 102782,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-26/1029-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-26/1029base-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-26/1029-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-26/1029ndvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 0.0,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-10-26",
            "suddenDrop": false,
            "hasCloud": false
        },
        {
            "id": 102779,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-30/0f3c-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-30/0f3cbase-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-30/0f3c-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-10-30/0f3cndvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 0.0,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-10-30",
            "suddenDrop": false,
            "hasCloud": false
        },
        {
            "id": 102793,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-11-01/1040-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-11-01/1040base-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-11-01/1040-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-11-01/1040ndvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 0.0,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-11-01",
            "suddenDrop": false,
            "hasCloud": false
        },
        {
            "id": 102808,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-11-03/1010-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-11-03/1010base-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-11-03/1010-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-11-03/1010ndvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 0.10304064409342624,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-11-03",
            "suddenDrop": false,
            "hasCloud": true
        },
        {
            "id": 102788,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-11-07/1010-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-11-07/1010base-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-11-07/1010-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-11-07/1010ndvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 1.0217218055867749E-5,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-11-07",
            "suddenDrop": false,
            "hasCloud": false
        },
        {
            "id": 102780,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-11-10/0f17-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-11-10/0f17base-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-11-10/0f17-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-11-10/0f17ndvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 1.22606616670413E-4,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-11-10",
            "suddenDrop": false,
            "hasCloud": false
        },
        {
            "id": 102781,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-11-14/103b-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-11-14/103bbase-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-11-14/103b-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-11-14/103bndvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 1.0217218055867749E-5,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-11-14",
            "suddenDrop": false,
            "hasCloud": false
        },
        {
            "id": 102791,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-11-26/101b-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-11-26/101bbase-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-11-26/101b-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-11-26/101bndvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 0.03598504199276621,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-11-26",
            "suddenDrop": false,
            "hasCloud": false
        },
        {
            "id": 102803,
            "asset": null,
            "image": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-12-03/0f46-base-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-12-03/0f46base-mosaic-thumbnail.png"
            },
            "ndviImage": {
                "reference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-12-03/0f46-ndvi-mosaic.png",
                "thumbnailReference": "https://s3.sa-east-1.amazonaws.com/prod-brasilseg-audsatplatform-images-engine-workflow-mosaic/3999/2019-12-03/0f46ndvi-mosaic-thumbnail.png"
            },
            "cloudPercentage": 0.9983141590207818,
            "hasBlackFill": false,
            "hasBaseGraphic": true,
            "date": "2019-12-03",
            "suddenDrop": false,
            "hasCloud": true
        }
    ],
    "status": "PENDING_TECHNICAL_ANALYSIS",
    "hasConfirmedArea": false,
    "hasConfirmedTemporalAnalysis": false
};
