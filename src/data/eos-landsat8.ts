export const eosLandsat8Bands = [
    {
        name: 'B1',
        flag: false
    },
    {
        name: 'B2',
        flag: false
    },
    {
        name: 'B3',
        flag: false
    },
    {
        name: 'B4',
        flag: true
    },
    {
        name: 'B5',
        flag: true
    },
    {
        name: 'B6',
        flag: true
    },
    {
        name: 'B7',
        flag: false
    },
    {
        name: 'B8',
        flag: false
    },
    {
        name: 'B9',
        flag: false
    },
    {
        name: 'B10',
        flag: false
    },
    {
        name: 'B11',
        flag: false
    },
]

export const eosLandsat8Images = [
	{
		"cloudCoverage": 0,
		"onAmazon": true,
		"id": "L8/LC08_L1TP_222072_20190907_20190917_01_T1",
		"productID": "LC08_L1TP_222072_20190907_20190917_01_T1",
		"storedInCollection": "01",
		"tms": "https://{a-d}-gate.eos.com/api/render/L8/LC08_L1TP_222072_20190907_20190917_01_T1/{band}/{z}/{x}/{y}",
		"sceneID": "LC82220722019250LGN00",
		"date": "2019-09-07"
	},
	{
		"cloudCoverage": 0,
		"onAmazon": true,
		"id": "L8/LC08_L1TP_221072_20190916_20190925_01_T1",
		"productID": "LC08_L1TP_221072_20190916_20190925_01_T1",
		"storedInCollection": "01",
		"tms": "https://{a-d}-gate.eos.com/api/render/L8/LC08_L1TP_221072_20190916_20190925_01_T1/{band}/{z}/{x}/{y}",
		"sceneID": "LC82210722019259LGN00",
		"date": "2019-09-16"
	},
	{
		"cloudCoverage": 0,
		"onAmazon": true,
		"id": "L8/LC08_L1TP_221073_20190916_20190925_01_T1",
		"productID": "LC08_L1TP_221073_20190916_20190925_01_T1",
		"storedInCollection": "01",
		"tms": "https://{a-d}-gate.eos.com/api/render/L8/LC08_L1TP_221073_20190916_20190925_01_T1/{band}/{z}/{x}/{y}",
		"sceneID": "LC82210732019259LGN00",
		"date": "2019-09-16"
	},
	{
		"cloudCoverage": 5.02,
		"onAmazon": true,
		"id": "L8/LC08_L1TP_222072_20190923_20190926_01_T1",
		"productID": "LC08_L1TP_222072_20190923_20190926_01_T1",
		"storedInCollection": "01",
		"tms": "https://{a-d}-gate.eos.com/api/render/L8/LC08_L1TP_222072_20190923_20190926_01_T1/{band}/{z}/{x}/{y}",
		"sceneID": "LC82220722019266LGN00",
		"date": "2019-09-26"
	},
	{
		"cloudCoverage": 0.14,
		"onAmazon": true,
		"id": "L8/LC08_L1TP_221073_20191002_20191018_01_T1",
		"productID": "LC08_L1TP_221073_20191002_20191018_01_T1",
		"storedInCollection": "01",
		"tms": "https://{a-d}-gate.eos.com/api/render/L8/LC08_L1TP_221073_20191002_20191018_01_T1/{band}/{z}/{x}/{y}",
		"sceneID": "LC82210732019275LGN00",
		"date": "2019-10-02"
	},
	{
		"cloudCoverage": 0.18,
		"onAmazon": true,
		"id": "L8/LC08_L1TP_221072_20191002_20191018_01_T1",
		"productID": "LC08_L1TP_221072_20191002_20191018_01_T1",
		"storedInCollection": "01",
		"tms": "https://{a-d}-gate.eos.com/api/render/L8/LC08_L1TP_221072_20191002_20191018_01_T1/{band}/{z}/{x}/{y}",
		"sceneID": "LC82210722019275LGN00",
		"date": "2019-10-02"
	},
	{
		"cloudCoverage": 50.59,
		"onAmazon": true,
		"id": "L8/LC08_L1TP_222072_20191009_20191018_01_T1",
		"productID": "LC08_L1TP_222072_20191009_20191018_01_T1",
		"storedInCollection": "01",
		"tms": "https://{a-d}-gate.eos.com/api/render/L8/LC08_L1TP_222072_20191009_20191018_01_T1/{band}/{z}/{x}/{y}",
		"sceneID": "LC82220722019282LGN00",
		"date": "2019-10-09"
	},
	{
		"cloudCoverage": 0.88,
		"onAmazon": true,
		"id": "L8/LC08_L1TP_221072_20191018_20191029_01_T1",
		"productID": "LC08_L1TP_221072_20191018_20191029_01_T1",
		"storedInCollection": "01",
		"tms": "https://{a-d}-gate.eos.com/api/render/L8/LC08_L1TP_221072_20191018_20191029_01_T1/{band}/{z}/{x}/{y}",
		"sceneID": "LC82210722019291LGN00",
		"date": "2019-10-29"
	},
	{
		"cloudCoverage": 1.56,
		"onAmazon": true,
		"id": "L8/LC08_L1TP_221073_20191018_20191029_01_T1",
		"productID": "LC08_L1TP_221073_20191018_20191029_01_T1",
		"storedInCollection": "01",
		"tms": "https://{a-d}-gate.eos.com/api/render/L8/LC08_L1TP_221073_20191018_20191029_01_T1/{band}/{z}/{x}/{y}",
		"sceneID": "LC82210732019291LGN00",
		"date": "2019-10-29"
	},
	{
		"cloudCoverage": 21.88,
		"onAmazon": true,
		"id": "L8/LC08_L1TP_222072_20191025_20191030_01_T1",
		"productID": "LC08_L1TP_222072_20191025_20191030_01_T1",
		"storedInCollection": "01",
		"tms": "https://{a-d}-gate.eos.com/api/render/L8/LC08_L1TP_222072_20191025_20191030_01_T1/{band}/{z}/{x}/{y}",
		"sceneID": "LC82220722019298LGN00",
		"date": "2019-10-30"
	},
	{
		"cloudCoverage": 21.52,
		"onAmazon": true,
		"id": "L8/LC08_L1TP_222072_20191110_20191115_01_T1",
		"productID": "LC08_L1TP_222072_20191110_20191115_01_T1",
		"storedInCollection": "01",
		"tms": "https://{a-d}-gate.eos.com/api/render/L8/LC08_L1TP_222072_20191110_20191115_01_T1/{band}/{z}/{x}/{y}",
		"sceneID": "LC82220722019314LGN00",
		"date": "2019-11-15"
	},
	{
		"cloudCoverage": 73.36,
		"onAmazon": true,
		"id": "L8/LC08_L1TP_221072_20191103_20191115_01_T1",
		"productID": "LC08_L1TP_221072_20191103_20191115_01_T1",
		"storedInCollection": "01",
		"tms": "https://{a-d}-gate.eos.com/api/render/L8/LC08_L1TP_221072_20191103_20191115_01_T1/{band}/{z}/{x}/{y}",
		"sceneID": "LC82210722019307LGN00",
		"date": "2019-11-15"
	},
	{
		"cloudCoverage": 26.01,
		"onAmazon": true,
		"id": "L8/LC08_L1TP_221073_20191103_20191115_01_T1",
		"productID": "LC08_L1TP_221073_20191103_20191115_01_T1",
		"storedInCollection": "01",
		"tms": "https://{a-d}-gate.eos.com/api/render/L8/LC08_L1TP_221073_20191103_20191115_01_T1/{band}/{z}/{x}/{y}",
		"sceneID": "LC82210732019307LGN00",
		"date": "2019-11-15"
	},
	{
		"cloudCoverage": 37.88,
		"onAmazon": true,
		"id": "L8/LC08_L1TP_222072_20191126_20191126_01_RT",
		"productID": "LC08_L1TP_222072_20191126_20191126_01_RT",
		"storedInCollection": "01",
		"tms": "https://{a-d}-gate.eos.com/api/render/L8/LC08_L1TP_222072_20191126_20191126_01_RT/{band}/{z}/{x}/{y}",
		"sceneID": "LC82220722019330LGN00",
		"date": "2019-11-26"
	},
	{
		"cloudCoverage": 27.46,
		"onAmazon": true,
		"id": "L8/LC08_L1TP_221073_20191119_20191203_01_T1",
		"productID": "LC08_L1TP_221073_20191119_20191203_01_T1",
		"storedInCollection": "01",
		"tms": "https://{a-d}-gate.eos.com/api/render/L8/LC08_L1TP_221073_20191119_20191203_01_T1/{band}/{z}/{x}/{y}",
		"sceneID": "LC82210732019323LGN00",
		"date": "2019-12-03"
	},
	{
		"cloudCoverage": 45.41,
		"onAmazon": true,
		"id": "L8/LC08_L1TP_221072_20191119_20191203_01_T1",
		"productID": "LC08_L1TP_221072_20191119_20191203_01_T1",
		"storedInCollection": "01",
		"tms": "https://{a-d}-gate.eos.com/api/render/L8/LC08_L1TP_221072_20191119_20191203_01_T1/{band}/{z}/{x}/{y}",
		"sceneID": "LC82210722019323LGN00",
		"date": "2019-12-03"
	}
]